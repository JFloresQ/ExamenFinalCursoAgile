﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger
{
    public interface IRepositorio
    {
        void LogMensajeArchivo(EntidadMensaje entidadMensaje);
        void LogMensajeConsola(EntidadMensaje entidadMensaje);
        void LogMensajeBaseDatos(EntidadMensaje entidadMensaje);
    }
}
