﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger
{
    public class Repositorio : IRepositorio
    {
        public void LogMensajeArchivo(EntidadMensaje entidadMensaje)
        {
            if (string.IsNullOrEmpty(entidadMensaje.mensajeLog.Trim()))
            {
                return;
            }

            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            connection.Open();

            SqlCommand command = new SqlCommand("Insert into Log Values('" + entidadMensaje.mensajeLog + "', " + Enumerado.TipoMensaje.Mensaje + ")");
            command.ExecuteNonQuery();
        }

        public void LogMensajeConsola(EntidadMensaje entidadMensaje)
        {
            string mensajeAGuardar = string.Empty;

            if (!File.Exists(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] + "ArchivoLog" + DateTime.Now.ToShortDateString() + ".txt"))
            {
                mensajeAGuardar += File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] + "ArchivoLog" + DateTime.Now.ToShortDateString() + ".txt");
            }

            mensajeAGuardar += DateTime.Now.ToShortDateString() + entidadMensaje.mensajeLog;

            File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] + "ArchivoLog" + DateTime.Now.ToShortDateString() + ".txt", mensajeAGuardar);
        }

        public void LogMensajeBaseDatos(EntidadMensaje entidadMensaje)
        {
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(DateTime.Now.ToShortDateString() + entidadMensaje.mensajeLog);
        }
    }
}
