﻿using System;
using System.Data.SqlClient;
using System.IO;
using MiLogger;

namespace Examen
{
    public class Logger
    {
        private static EntidadMensaje _EntidadMensaje;
        private static IRepositorio _IRepositorio;

        public Logger(IRepositorio IRepositorio, EntidadMensaje pEntidadMensaje)
        {
            _IRepositorio = IRepositorio;
            _EntidadMensaje = pEntidadMensaje;
        }

        public void Mensaje()
        {
            _EntidadMensaje.tipoMensaje = Enumerado.TipoMensaje.Mensaje;
            _EntidadMensaje.color = Enumerado.Color.White;
            _IRepositorio.LogMensajeArchivo(_EntidadMensaje);
        }

        public void Alerta()
        {
            _EntidadMensaje.tipoMensaje = Enumerado.TipoMensaje.Alerta;
            _EntidadMensaje.color = Enumerado.Color.Yellow;
            _IRepositorio.LogMensajeArchivo(_EntidadMensaje);
        }

        public void Error()
        {
            _EntidadMensaje.tipoMensaje = Enumerado.TipoMensaje.Error;
            _EntidadMensaje.color = Enumerado.Color.Red;
            _IRepositorio.LogMensajeArchivo(_EntidadMensaje);
        }
        
        //public void LogMensaje()
        //{
        //    var _IRepositorio = CreateState((int)_EntidadMensaje.tipoMensaje);
        //    _IRepositorio.LogMensaje(_EntidadMensaje.mensajeLog);
        //}

        //private IRepositorio CreateState(int tipoMensaje)
        //{
        //    _IRepositorio = null;

        //    if (tipoMensaje == (int)Enumerado.TipoMensaje.Mensaje)
        //    {
        //        _IRepositorio = new RepositorioMensaje();
        //    }
        //    else if (tipoMensaje == (int)Enumerado.TipoMensaje.Alerta)
        //    {
        //        _IRepositorio = new RepositorioAlerta();
        //    }
        //    else if (tipoMensaje == (int)Enumerado.TipoMensaje.Error)
        //    {
        //        _IRepositorio = new RepositorioError();
        //    }

        //    return _IRepositorio;
        //}
    }
}
