﻿using Examen;
using FluentAssertions;
using MiLogger;
using Moq;
using NUnit.Framework;

namespace MiLoggerTest
{
    [TestFixture]
    class LoggerTest
    {
        private Logger _logger;
        private EntidadMensaje _EntidadMensaje;
        private Mock<IRepositorio> myDataAccess;

        [SetUp]
        public void Setup()
        {
            myDataAccess = new Mock<IRepositorio>();
        }

        [Test]
        public void Logger_CrearLogger()
        {
            _EntidadMensaje = new EntidadMensaje
            {
                mensajeLog = "",
            };

            _logger = new Logger(myDataAccess.Object, _EntidadMensaje);
            _logger.Should().NotBeNull();
        }

        [Test]
        public void Logger_InsertarLogMensaje()
        {
            _EntidadMensaje = new EntidadMensaje
            {
                mensajeLog = "Mensaje",
            };

            myDataAccess.Setup(m => m.LogMensajeArchivo(_EntidadMensaje));

            _logger = new Logger(myDataAccess.Object, _EntidadMensaje);
            _logger.Mensaje();
        }

        [Test]
        public void Logger_InsertarLogAlerta()
        {
            _EntidadMensaje = new EntidadMensaje
            {
                mensajeLog = "Alerta",
            };

            myDataAccess.Setup(m => m.LogMensajeArchivo(_EntidadMensaje));

            _logger = new Logger(myDataAccess.Object, _EntidadMensaje);
            _logger.Alerta();
        }

        [Test]
        public void Logger_InsertarLogError()
        {
            _EntidadMensaje = new EntidadMensaje
            {
                mensajeLog = "Error",
            };

            myDataAccess.Setup(m => m.LogMensajeArchivo(_EntidadMensaje));

            _logger = new Logger(myDataAccess.Object, _EntidadMensaje);
            _logger.Error();
        }
    }
}
