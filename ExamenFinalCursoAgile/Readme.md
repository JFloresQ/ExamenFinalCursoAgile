#Instrucciones

Revise el codigo de la clase Logger.
Debe asumir que todas la referencias estan configuradas correctamente.

La clase es usada para logear diferentes mensajes/eventos de una aplicaci�n
Se desea la habilidad de logear en un archivo de texto, consola, y en una base de datos.
Cada mensaje puede tener diferentes niveles, Alerta, Error, Informacion.
Ademas se desea la habilidad de selectivamente poder escoger donde logear, 
(Archivo, Consola, Base de datos) asi como determinar el nivel del tipo mensaje.

1. Refactorize el codigo de la clase, de forma que cumpla lo siguientes:
* Mantenibilidad
* Poco Acoplamiento.
* Respetar los principios Solid.
* C�digo limpio.
* Escribir las pruebas unitarias que verifiquen/validen el comportamiento.

2. Debe configurar un proyecto en teamcity para ejecutar la compilacion y la ejecuci�n de las pruebas unitarias.
   Debe capturar las pantallas de la ejecuci�n de builds.


Criterios de entrega.

Debera subir el codigo fuente al repositorio que se le indique en el salon
con la siguiente estructura.
Equipo
   src (carpeta para el codigo fuente)
   docs (evidencia de CI)

